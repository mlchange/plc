import getapi
import pyads
import time

def pushdate(Hashrate,T1,T2,T3,Chip_Temp_min,Chip_temp_max,Chip_temp_avg):
    pyads.open_port()
    sending_net_id = '5.86.110.238.1.1'
    adding_host_name = '192.168.3.138'
    ip_address = '183.221.119.59'
    username = 'Administrator'
    password = ''
    route_name = 'raspberry'
    added_net_id = '192.168.3.138.1.1'
    pyads.add_route_to_plc(sending_net_id,adding_host_name,ip_address,username,password,route_name,added_net_id)
    pyads.add_route('5.86.110.238.1.1', '183.221.119.59')
    pyads.set_local_address('192.168.3.138.1.1')

    plc = pyads.Connection('5.86.110.238.1.1' , 801 , '183.221.119.59')
    plc.open()
    plc.write_by_name('cpu.data[0].Hashrate',Hashrate, pyads.PLCTYPE_REAL)
    plc.write_by_name('cpu.data[0].Temperature.T1',T1, pyads.PLCTYPE_REAL)
    plc.write_by_name('cpu.data[0].Temperature.T2',T2, pyads.PLCTYPE_REAL)
    plc.write_by_name('cpu.data[0].Temperature.T3',T3, pyads.PLCTYPE_REAL)
    plc.write_by_name('cpu.data[0].Temperature.Chip_Temp_min',Chip_Temp_min, pyads.PLCTYPE_REAL)
    plc.write_by_name('cpu.data[0].Temperature.Chip_temp_max',Chip_temp_max, pyads.PLCTYPE_REAL)
    plc.write_by_name('cpu.data[0].Temperature.Chip_temp_avg',Chip_temp_avg, pyads.PLCTYPE_REAL)
    plc.close()

def get_data(command):
    getapi.exec_command("get_token", onlyread = True)
    getapi.exec_command(command,onlyread = True)
    from getapi import get_global
    data_recv = get_global()
    return data_recv



if __name__ == '__main__':
    data_recv = get_data('summary')
    Hashrate  =  '%.2f' %(float(data_recv['SUMMARY'][0]['MHS av'])/1000000)
    Chip_Temp_min = '%.2f' %data_recv['SUMMARY'][0]['Chip Temp Min']
    Chip_temp_max = '%.2f' %data_recv['SUMMARY'][0]['Chip Temp Max']
    Chip_temp_avg = '%.2f' %data_recv['SUMMARY'][0]['Chip Temp Avg']
    time.sleep(10)
    data_recv = get_data('edevs')
    T1 = '%.2f' %data_recv['DEVS'][0]['Temperature']
    T2 = '%.2f' %data_recv['DEVS'][1]['Temperature']
    T3 = '%.2f' %data_recv['DEVS'][2]['Temperature']
    print(T1,T2,T3)
    #print(data_recv)
    pushdate(float(Hashrate),float(T1),float(T2),float(T3),float(Chip_Temp_min),float(Chip_temp_max),float(Chip_temp_avg))
   
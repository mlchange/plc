import socket
import json
import sys
import os
import struct
import os.path
from os import walk
import base64
import select
from hashlib import md5
import passlib.hash
from passlib.hash import md5_crypt
import re
import sys
from ctypes import *
import string
import hashlib
from Crypto.Cipher import AES
import binascii
from binascii import b2a_hex
from binascii import a2b_hex
import datetime
import time
from datetime import datetime
from datetime import timedelta
import configparser



#host_ip = 0
host_sign = 0
host_passwd_md5 = 0
host_tcp_port = 0
host_token = 0
data_recv = 0

def add_to_16(s):
    while len(s) % 16 != 0:
        s += '\0'
    return str.encode(s)  # return bytes

def recv_response(s, timeout=15):
    s.setblocking(False)
    ready = select.select([s], [], [], timeout)
    if ready[0]:
        data = s.recv(1024 * 8)
        return data
    else:
        raise Exception('socket recv timeout')
        return False    
        
def recv_file(s, length, name, timeout=8):
    print('ready recv name:%s len:%x' % (name,length))
    fo = open(name, "wb+")
    filelen = length
    while True:
        s.setblocking(False)
        ready = select.select([s], [], [], timeout)
        if ready[0]:
            data = s.recv(filelen)
            fo.write(data)
            filelen -= len(data)
            if filelen == 0:
                break;
        else:
            raise Exception('socket recv timeout')
            return False
    fo.close()
    return True
    
# md5-crypt
def crypt(word, salt):
    standard_salt = re.compile('\s*\$(\d+)\$([\w\./]*)\$')
    match = standard_salt.match(salt)
    if not match:
        raise ValueError("salt format is not correct")
    extra_str = match.group(2)
    entryptor = passlib.hash.md5_crypt
    result = entryptor.encrypt(word, salt=extra_str)
    return result

def exec_command(api_cmd, json_param=None, sz_file=None, file_dat=None, onlyread=False):
#    global host_ip
    global host_tcp_port
    global host_sign
    global host_passwd_md5
    global host_token
    global data_recv


    api_packet_str = api_cmd
    #create json string
    api_json = {"command": api_cmd} #{"cmd": api_cmd} is ok also
    if json_param != None:
        api_json.update(json_param)
    if onlyread == False:
        api_json['token'] = host_sign
    api_json_str = json.dumps(api_json)
    print("(JSON CMD)%s" % api_json_str)

    #encode json string
    api_packet_str = api_json_str
    if onlyread == False:
        print('(API,PWD,SIGN) (%s,%s,%s)' %
                (api_json_str, host_passwd_md5, host_sign))
        if host_sign != 0:
            aeskey = hashlib.sha256(host_passwd_md5.encode()).hexdigest()
            aeskey = binascii.unhexlify(aeskey.encode())
            aes = AES.new(aeskey, AES.MODE_ECB)
            api_json_str_enc = str(base64.encodebytes(
                aes.encrypt(add_to_16(api_json_str))),
                                    encoding='utf8').replace('\n', '')
            data_enc = {'enc': 1}
            data_enc['data'] = api_json_str_enc
            api_packet_str = json.dumps(data_enc)
            print("(ENC CMD) %s" % api_packet_str)
        else:
            raise Exception('token error：token is none')
            return

    #create socket & send
    host_ip = '192.168.3.136'
    #set_config(host_ip)
    host_tcp_port = '4028'
    #socket.setdefaulttimeout(2)#FIXME:
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    #s.setblocking(False)
    try:
        s.connect((host_ip, int(host_tcp_port)))
    except socket.timeout as e:
        raise Exception('socket error:tcp timeout')
        return

    start_time = time.time()# TODO: 
    s.send(api_packet_str.encode())
    print("(TCP SENT)%s" % api_packet_str)

    #receive and process whatsminer return msg
    #response = s.recv(1024 * 4)
    response = recv_response(s)
    print("(TCP RECV)%s" % response)
    if api_cmd == "update_firmware":
        ret_msg = response
        try:
            d = json.loads(ret_msg)
            ret_msg = d["enc"].encode()
        except:
            raise Exception('update_firmware error:response error')
            s.close()
            return

        crypt_data = ret_msg.decode()
        aeskey = hashlib.sha256(host_passwd_md5.encode()).hexdigest()
        aeskey = binascii.unhexlify(aeskey.encode())
        aes = AES.new(aeskey, AES.MODE_ECB)
        ret_msg = str(
            aes.decrypt(
                base64.decodebytes(bytes(
                    crypt_data,
                    encoding='utf8'))).rstrip(b'\0').decode("utf8"))
        print("(FW decrypted_text):%s" % ret_msg)

        try:
            d = json.loads(ret_msg)
            ret_msg = d["Msg"].encode()
        except:
            s.close()
            return

        if ret_msg.decode() != 'ready':
            raise Exception('error:response error')
            s.close()
            return
        print("(UPDATE FW) ready ok")
        s.send(sz_file)
        s.send(file_dat)
        response = recv_response(s)
    elif api_cmd == "download_logs":
        ret_msg = response
        try:
            d = json.loads(ret_msg)
            ret_msg = d["enc"].encode()
        except:
            raise Exception('download_logs error:response error')
            s.close()
            return

        crypt_data = ret_msg.decode()
        aeskey = hashlib.sha256(host_passwd_md5.encode()).hexdigest()
        aeskey = binascii.unhexlify(aeskey.encode())
        aes = AES.new(aeskey, AES.MODE_ECB)
        ret_msg = str(
            aes.decrypt(
                base64.decodebytes(bytes(
                    crypt_data,
                    encoding='utf8'))).rstrip(b'\0').decode("utf8"))
        print("(logs decrypted_text):%s" % ret_msg)
        '''
        try:
            d = json.loads(ret_msg)
            msg = d["Msg"]
            recv_file(s, int(msg['logfilelen']), name=str(t_mac.get()) + datetime.now() +'.tgz')
        except:
            print('download_logs recv data error')
            s.close()
            return
        '''
        d = json.loads(ret_msg)
        msg = d["Msg"]
        dt=datetime.now()
        ymd=dt.strftime('%Y%m%d%H%M%S')
        recv_file(s, int(msg['logfilelen']), name=str(host_ip) + '.' + ymd +'.tgz')

    s.close()
    end_time = time.time()  #TODO:
    print("start_time:\t%s" % start_time)
    print("end_time:\t%s" % end_time)
    print("time:\t%s" % ((end_time - start_time) * 1000))
    if response == False:
        raise Exception('tcp recv error:whatsminer response none')

    #parse ret msg
    ret_msg = response
    label_ret_msg['text'] = ''
    if onlyread == True:
        if api_cmd == "get_token":  #generate key and token sign:
            try:
                r = json.loads(ret_msg)
                d = r['Msg']
            except:
                raise Exception('get token error:is not json')
            else:
                if isinstance(d, dict):
                    passwd = 'admina'
                    print("(PWD, SALT,TIME,NEWSALT) %s, %s,%s,%s" % (passwd,d["salt"],d["time"],d["newsalt"]))
                    pwd = crypt(passwd, "$1$" + d["salt"] + '$')
                    pwd = pwd.split('$')
                    host_passwd_md5 = pwd[3]

                    tmp = crypt(pwd[3] + d["time"], "$1$" + d["newsalt"] + '$')
                    tmp = tmp.split('$')
                    host_sign = tmp[3]
                    host_token = d["time"] + ',' + d["newsalt"] + ',' + host_sign
                    print(
                        '(PWD,MD5,SIGN) %s, %s, %s'
                        % (passwd, host_passwd_md5, host_sign))
    else:
        if len(ret_msg) < 256: #FIXME: json > 256Byte will crash
            try:
                d = json.loads(ret_msg)
            except:
                raise Exception('error:load json')
            else:
                if 'enc' in d.keys():
                    enc_data = d['enc']
                    aeskey = hashlib.sha256(host_passwd_md5.encode()).hexdigest()
                    aeskey = binascii.unhexlify(aeskey.encode())
                    aes = AES.new(aeskey, AES.MODE_ECB)
                    ret_msg = str(
                        aes.decrypt(base64.decodebytes(bytes(
                            enc_data, encoding='utf8'))).rstrip(b'\0').decode("utf8"))
        else:
            pass#TODO: The privilege API respone msg less then 256Bytes generally.
    label_ret_msg['text'] = ret_msg
    data_recv = json.loads(ret_msg)
    print(ret_msg)

def get_global():
    global data_recv
    return data_recv


global label_ret_msg
label_ret_msg = {}

if __name__ == '__main__':
    exec_command("get_token", onlyread = True)
    exec_command('summary',onlyread = True)
    a  =  '%.2f' %data_recv['SUMMARY'][0]['MHS av']
    print(a)
    
    #exec_command('reboot')
    


